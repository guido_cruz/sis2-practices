public class Meteo {    //Clima
    
    public static void main(String[] args) {
        //prueba los métodos de la clase ArrayTools
        //prueba de los métodos de la clase ToolsTableaux
        OutilsTableaux outilT = new OutilsTableaux();   //herramienta T
        double[] t =  outilT.creerTableau(10);
        System.out.println("Affichage du tableau");     //Exhibición de la tabla
        System.out.println(outilT.contenuTab(t));
        
        //Preguntas de la sección 1.1
        double [] tabTrieSelect = outilT.triSelection(t);
        System.out.println(outilT.contenuTab(tabTrieSelect));
        double[] tabTrieBulle = outilT.triBulle(t);
        System.out.println(outilT.contenuTab(tabTrieBulle));
        
        
        // Creación de instancias de MeteoVille
        // temperaturas recogidas por un sensor en Toulon
        double[] temp1 = {8.2, 10.3, 14.5,20.1, 26.4, 30.5, 28.7,24.2,18.5,13.2,8.3,5.7};
        //Tasa de humedad recogida por un sensor en Toulouse
        double[] hum1 = {0.3, 0.35, 0.5, 0.6, 0.7, 0.3, 0.2, 0.1, 0.2, 0.4, 0.5, 0.35};
        
        MeteoVille mV1 = new MeteoVille("Toulon", 372190248.4, temp1 , hum1);
        String imprimir = mV1.toString();
        System.out.print(imprimir);
        //muestra como resultado el valor del volume CO2 en valor scientifico: 3.721902484E8 
        //por que el numero es muy grande para el tipo primitivo double.
        //el valor primitivo que mejor muestra el resultado es el int

        //temperaturas recogidas por un sensor en Brest
        double[] temp2 = {8.2, 10.3, 11.5,16.1, 20.4, 22.5, 21.7,19.2,15.5,10.2,9.3,8.7};
        //Tasa de humedad recogida por un sensor en Brest
        double[] hum2 = {0.67, 0.69, 0.72, 0.62, 0.77, 0.78, 0.45, 0.69, 0.78, 0.85, 0.95,0.72};
        MeteoVille mV2 = new MeteoVille("Gap", 184151985.6, temp2 , hum2);
        
        //temperaturas recogidas por un sensor en Vitrolles
         double[] temp3 = {5.3, 6.3, 9.3,12, 15.8, 19.7, 22.2,21.8,18.8,14.4,9.5,6};
        //Tasa de humedad recogida por un sensor en Vitrolles
        double[] hum3 = {0.3, 0.35, 0.5, 0.6, 0.7, 0.3, 0.2, 0.1, 0.2, 0.4, 0.5, 0.35};
        MeteoVille mV3 = new MeteoVille("Vitrolles", 18008414.4, temp3 , hum3);
        
        //temperaturas recogidas por un sensor en Arles
        double[] temp4 = {6,5, 7.6, 10,12.3, 15.9, 19.6, 22.4,22.2,19.7,15.1,10.6,7.4};
        //Tasa de humedad recogida por un sensor en Arles
        double[] hum4 = {0.18, 0.16, 0.15, 0.2, 0.15, 0.1, 0.05, 0.1, 0.15, 0.24, 0.22, 0.15};
        MeteoVille mV4 = new MeteoVille("Arles",  725020969.9 , temp4 , hum4);
        
        
        //question 2.1
        System.out.println(mV1);
        
        //Question 2.2
        mV1.nouvellesDonnees(8.3,0.35);
        System.out.println(mV1);
        
        //prueba de cálculo mediana(question 2.3)
        System.out.print("mediane des températures de "+ mV1.getNomMeteoVille()+ " ");
        System.out.println(mV1.calculerMedianeTemp());
        
        
        
        System.out.println("\n\n************* Ejercicio 3 ***************\n\n");
        //Question 3
        MeteoVille mV5 = new MeteoVille("ciudad1", 372190248.4, temp1 , hum1);
        MeteoVille mV6 = new MeteoVille("Ciudad2", 372190248.4, temp1 , hum1);
        MeteoVille mV7 = new MeteoVille("Ciudad3", 372190248.4, temp1 , hum1);
        
        //Question 3
        MeteoVille[] villes = {mV5,mV6,mV7};
        //section 1.5
        MeteoPays france = new MeteoPays("France", villes);
        System.out.println (france);
        
        //test de la question 
        System.out.println("L'émission annuelle de "+ france.getNomPays() + " est de "+france.getEmissionCO2());
        System.out.println("La edición anual de "+ france.getNomPays() + "es de"+france.getEmissionCO2());
        
        france.classementVilleParEmission();
        
    }

}
