public class OutilsTableaux {   //Tablas de herramientas
    //Atributos
    //la clase no tiene atributos
     
    //Constructor
    public OutilsTableaux() {  //Tablas de herramientas
         
    }
     
    //método de visualización de tabla
    public String contenuTab(double[] tab){ //Contenido de la pestaña
        String res= "";
        for (int i=0; i<tab.length-1;i++){
            res = res + tab[i] + " | ";
        }
        res= res+tab[tab.length-1];
        return res;
    }
     
    //Método que crea una matriz de longitud n con valores elegidos al azar
    public double [] creerTableau(int n)  {     //crear tabla
        double [] tab = new double[n];
        for (int  i = 0; i<tab.length; i++) {
            tab[i]=Math.random()*100;
        }
        return tab;
    }
     
     
     
    //Question 1.1.1
    public double[] copieTableau(double[] tableau) {    //Copiar tabla
        double[] copia = new double[tableau.length];
        for (int  i = 0; i<tableau.length; i++) {
            copia[i] = tableau[i];
        }
        return copia;
    }
     
    
    
    //Question 1.1.2
    public double [] triSelection(double[] tableau) {      //Ordenar selección
        
        double[] tabAux = copieTableau(tableau);
        
        // Inicialización de los contadores de viaje y el número de permutaciones.
        int numeroCaminosRequeridos;//numero cambios realizados ????
        int numeroPermutacionesRequeridas;
        
        //clasificación
        for (int i = 0; i < tabAux.length - 1; i++)  
        {
            int posicionDelNumeroMenor = i;
            for (int j = i + 1; j < tabAux.length; j++){  
                if (tabAux[j] < tabAux[i]){  
                    posicionDelNumeroMenor = j;//searching for lowest index
                    //cambio de posiciones.
                    double auxiliar = tabAux[posicionDelNumeroMenor];   
                    tabAux[posicionDelNumeroMenor] = tabAux[i];  
                    tabAux[i] = auxiliar;
                }  
            }  
        }  
        //muestra el resultado de la pregunta 1.1.4
        //Para completar
        return tabAux;
    }
    
    
    //Método de clasificación de bulle
    public  double [] triBulle(double[] tableau) {  //ordenamiento de burbuja
        double[] tabAux = copieTableau(tableau);
        //Inicialización de los contadores de viaje y el número de permutaciones.
        int numeroCaminosRequeridos;//numero cambios realizados ????
        int numeroPermutacionesRequeridas;
        
        double variableAuxiliar;
        int ultimoElemento = tabAux.length - 1;
        boolean trie = false;
        int numeroDePasadas = 0;
        //Mientras la matriz no esté ordenada 
        while(!trie){
            // Para cualquier par de elementos adyacentes a y b
            int numeroDeCambios = 0;
            for (int i = 0; i < ultimoElemento; i++) {
                // si las dos posiciones no están ordenadas, cámbielas
                if (tabAux[i] > tabAux[i+1]) {
                    //cambiamos
                    numeroDeCambios = numeroDeCambios + 1;
                    variableAuxiliar = tabAux[i];
                    tabAux[i] = tabAux[i+1];
                    tabAux[i+1] = variableAuxiliar;
                }
                
            }
            //Comprueba si la matriz resultante está ordenada
            if(numeroDeCambios == 0) {
                trie = true;
            }
            numeroDePasadas = numeroDePasadas + 1;
        }
        return tabAux;
    }
}

