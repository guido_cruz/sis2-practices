public class MeteoPays {    //Clima del país
	
	 private String nomPays;        //nombre país
	 private MeteoVille[] villes;   //ciudades
	 
	 public MeteoPays(String nom, MeteoVille[] villes) {    //Clima del país
		this.nomPays= nom;
		this.villes=villes;
	 }
	 
	 public String toString() {     //Encadenar
		String s="Pays " + nomPays + "\n";
		for (int i= 0; i<villes.length;i++) {
			s=s+villes[i]+"\n";
		}
		//s=s+"Emisiones anuales de CO2"+getEmissionCO2()+"\n";
		return s; 
		
	 }
	 
	 public String getNomPays() {   //obtener el nombre del país
		return nomPays;
	 }
	 
	 public double getEmissionCO2() {   //obtener emisiones de CO2
             double totalEmisionesCO2 = 0;
             for (int i = 0; i < this.villes.length; i++) {
                 totalEmisionesCO2 = totalEmisionesCO2 + villes[i].getVolumeCo2();
             }
             return totalEmisionesCO2;
	 }
	 
	 
	 
	 public void classementVilleParEmission() {     //Clasificación de la ciudad por tema
		//A completer

            MeteoVille[] tabAux = this.villes;

            // Inicialización de los contadores de viaje y el número de permutaciones.
            int numeroCaminosRequeridos;//numero cambios realizados ????
            int numeroPermutacionesRequeridas;

            //clasificación
            for (int i = 0; i < tabAux.length - 1; i++)  
            {
                int posicionDelNumeroMenor = i;
                for (int j = i + 1; j < tabAux.length; j++){  
                    if (tabAux[j].getVolumeCo2() < tabAux[i].getVolumeCo2()){  
                        posicionDelNumeroMenor = j;//searching for lowest index
                        //cambio de posiciones.
                        MeteoVille auxiliar = tabAux[posicionDelNumeroMenor];   
                        tabAux[posicionDelNumeroMenor] = tabAux[i];  
                        tabAux[i] = auxiliar;
                    }  
                }  
            }  
            //muestra el resultado de la pregunta 1.1.4
            //Para completar
            for (int i = 0; i < tabAux.length; i++) {
                System.out.println("Ciudad: " + tabAux[i].getNomMeteoVille() + " Emision CO2: " + tabAux[i].getVolumeCo2());
            }
	 }
}

