public class MeteoVille {   //Clima de la ciudad
	
    private String nomMeteoVille;   //nombre de ClimaDeLaCiudad
    private double volumeCo2;       //Volumen de CO2 emitido
    private double[] tempMois ;     //tabla de temperatura promedio mensual
    private double[] tHumidite;     //tabla de niveles de humedad mensuales promedio
	
    // constructores
    //A definir

    public MeteoVille(String nomMeteoVille, double volumeCo2) {
        this.nomMeteoVille = nomMeteoVille;
        this.volumeCo2 = volumeCo2;
        this.tempMois = new double[12];
        this.tHumidite = new double[12];
    }

    public MeteoVille(String nomMeteoVille, double volumeCo2, double[] tempMois, double[] tHumidite) {
        this.nomMeteoVille = nomMeteoVille;
        this.volumeCo2 = volumeCo2;
        this.tempMois = tempMois;
        this.tHumidite = tHumidite;
    }
    
    // getters
    public String getNomMeteoVille(){
        return this.nomMeteoVille;
    }

    public double getVolumeCo2() {
        return volumeCo2;
    }
    
    public String toString() {     //Encadenar
        //Toulon :
        String nomVille = this.nomMeteoVille + ":\n";
        
        //Temperatures:
          //9.5 | 15.2 | 16.4 | 22.1 | 28.7 | 30.3 | 23.4 | ...
        OutilsTableaux outilsTableAux = new OutilsTableaux();
        String temp = "Temperatures :\n";
        temp = temp + outilsTableAux.contenuTab(this.tempMois);
        temp = temp + "\n";
     
        //Humidite:
        //0.3 * 0.35 * 0.5 * 0.6 * 0.7 * ...
        String humidite = "Taux d'Humidite :\n";
        for (int i=0; i < this.tHumidite.length-1;i++){
            humidite = humidite + this.tHumidite[i] + " * ";
        }
        humidite = humidite+this.tHumidite[this.tHumidite.length-1];
        humidite = humidite + "\n";
        
        //Emission annuelle de CO2: 372190248.4 kg
        String volumenEmisiones =  "Emission annuelle de CO2: " + this.volumeCo2 + " kg";
        
        String resultado = nomVille + temp + humidite + volumenEmisiones + "\n";
        return resultado;
    }
    
    //Question 1.2
    public void nouvellesDonnees(double temp, double tHum) {    //nuevos datos
        for (int i = 0; i < this.tempMois.length-1; i++) {
            this.tempMois[i] = this.tempMois[i+1];
        }
        for (int i = 0; i < this.tHumidite.length-1; i++) {
            this.tHumidite[i] = this.tHumidite[i+1];
        }
        this.tempMois[this.tempMois.length-1] = temp;
        this.tHumidite[this.tHumidite.length-1] = tHum;
   }
    
    public double calculerMedianeTemp(){        //calcular la temperatura media
        double mediana;
        
        OutilsTableaux herramienta = new OutilsTableaux();
        
        double[] tablaOrdenada = herramienta.triBulle(this.tempMois);
        
        int tamanioTabla = tablaOrdenada.length;
        //impar
        if(tamanioTabla%2 != 0){
            int posicionDeLaMediana = ((tamanioTabla + 1)/2)-1;
            mediana = tablaOrdenada[posicionDeLaMediana];
        }//par
        else {
            int posicionDeLaMediana = (tamanioTabla/2)-1; //menos 1 es por que los indices empiezan en 0            
            mediana = (tablaOrdenada[posicionDeLaMediana] + tablaOrdenada[posicionDeLaMediana+1])/2;
        }
        return mediana ;
    }	
}

